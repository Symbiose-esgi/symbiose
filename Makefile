#############
## CONFIGS ##
#############

include .env

MAKEFLAGS += --no-print-directory

uname_S := $(shell sh -c 'uname -s 2>/dev/null || echo not')
ifeq ($(uname_S),Linux)
	HOSTESS_ARCH = linux_amd64
else ifeq ($(uname_S),Darwin)
	HOSTESS_ARCH = darwin_amd64
endif

HOSTESS_URL = https://github.com/cbednarski/hostess/releases/download/v0.3.0/hostess_$(HOSTESS_ARCH)
HOSTESS_BIN	= ./bin/hostess

#############
### ALIAS ###
#############

u: up
compi: composer-install
compu: composer-update
cons-l: console-list
cons-me: console-make-entity
d: down
i: init
h: hostess
r: restart

#############
## COMMAND ##
#############

down:
	docker-compose down

restart: down up

init: hostess env composer-install

composer-install:
	docker-compose run composer composer install

composer-update:
	docker-compose run composer composer update

hostess:
ifeq ($(shell [ -f $(HOSTESS_BIN) ] && echo 1 || echo 0 ),0)
	curl $(HOSTESS_URL) -o $(HOSTESS_BIN) -L
	chmod +x $(HOSTESS_BIN)
endif
	sudo $(HOSTESS_BIN) apply ./hosts.json

env:
ifeq ($(shell [ -f .env ] && echo 1 || echo 0 ),0)
	cp .env.dist .env
endif

console-list:
	docker exec $(DOCKER_PREFIX)php ./bin/console list

console-make-entity:
	docker exec $(DOCKER_PREFIX)php ./bin/console make:entity

up:
	docker-compose up --build

sql:
	docker exec $(DOCKER_PREFIX)php php ./bin/console d:s:u --force