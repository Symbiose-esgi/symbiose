# Symbiose

## Requirements

- Make
- Docker
- docker-compose
- sudo (for Linux & Mac)

## How to run project ?

- Init project : 
    - If you're on Linux or Mac, just run `make init`
    - If you're on Windows, run ` make env && make composer-install` and add those 2 lines in your `hosts` : 
        ```
        127.0.0.1 adminer.test
        127.0.0.1 symbiose.test
        ```
- Run `make` or `make up` to run project

## Web apps
- For main app, go to `http://symbiose.test`
- For adminer, go to `http://adminer.test`
