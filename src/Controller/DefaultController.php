<?php

// C'est quoi un namespace ?
namespace App\Controller;

// Inclusion d'une librarie
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swift_Mailer;
use App\Form\ContactAdminType;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;

/*
 * @Route
 */


class DefaultController extends AbstractController
{
    /**
     * @Route(name="app_default_index", path="/", methods={"GET"})
     * @return Response
     */
    public function index()
    {
        return $this->render('landing/index.html.twig');
    }

    /**
     * @Route("/offers", name="app_offers")
     */
    public function offers()
    {
        return $this->render('informations/offers.html.twig');
    }

    /**
     * @Route("/legal-notices", name="app_legalnotices")
     */
     public function legalNotices()
     {
         return $this->render('informations/legal_notices.html.twig');
     }

    /**
     * @Route("/faq", name="app_faq")
     */
    public function faq(Request $request, PaginatorInterface $paginator)
    {
        $faqContents = file_get_contents('../templates/help/contact.json', true);

        $faq = $paginator->paginate(
            json_decode($faqContents, true),
            $request->query->getInt('page', 1),
            4
        );

        return $this->render('help/faq.html.twig', array('faqs' => $faq));
    }

    /**
     * @Route("/contact", name="app_contact_admin")
     */
    public function contact_admin(Request $request, Swift_Mailer $mailer)
    {

        $form = $this->createForm(ContactAdminType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {//SI le formulaire est bien rempli
            //$request->request->get('contact_admin')["email"]
            $message = (new \Swift_Message('Symbiose'))
                ->setSubject($request->request->get('contact_admin')["subject"])
                ->setFrom($request->request->get('contact_admin')["email"])
                ->setTo('symbiose.esgi@gmail.com')
                ->setBody($request->request->get('contact_admin')["message"]);

            $mailer->send($message);
            return $this->render(
                'help/contact-admin.twig',
                array('form' => $form->createView(), 'message' => 'Nous allons recevoir votre mail.')
            );
        }

        return $this->render(
            'help/contact-admin.twig',
            array('form' => $form->createView(), 'message' => null)
        );


    }
}