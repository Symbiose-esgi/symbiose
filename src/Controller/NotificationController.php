<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Repository\NotificationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/notification")
 */
class NotificationController extends AbstractController
{
    /**
     * @Route("/", name="notification_index", methods={"GET"})
     */
    public function index(NotificationRepository $notificationRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $allNotification = $paginator->paginate(
            $notificationRepository->findPersonalAll($this->getUser()),
            $request->query->getInt('page', 1),
            5
        );

        //$allNotification = $notificationRepository->findPersonalAll($this->getUser());
        $delayRender = $this->render('notification/index.html.twig', [
            'notifications' => $allNotification,
        ]);
        $entityManager = $this->getDoctrine()->getManager();

        foreach ($allNotification as $notification){
            $notification->setIsSeen(1);
        }
        $entityManager->flush();

        return $delayRender;
    }

    /**
     * @Route("/{id}", name="notification_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Notification $notification): Response
    {
        if ($this->isCsrfTokenValid('delete'.$notification->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($notification);
            $entityManager->flush();
        }

        return $this->redirectToRoute('notification_index');
    }
}
