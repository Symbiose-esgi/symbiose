<?php

namespace App\Controller;

use App\Entity\Profil;
use App\Form\ProfilType;
use App\Repository\ProfilRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use \Datetime;

/**
 * @Route("/profil")
 */
class ProfilController extends AbstractController
{
    /**
     * @Route("/", name="profil_index", methods={"GET"})
     */
    public function index(ProfilRepository $profilRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $profils = $paginator->paginate(
            $profilRepository->findAllProfileCollab(),
            $request->query->getInt('page', 1),
            5
        );

        $profiles = $profilRepository->findProfileRecruteur($this->getUser()->getId());

        return $this->render('profil/index.html.twig', ['profils' => $profils, 'profiles' => $profiles]);
    }

    /**
     * @Route("/myprofil", name="myprofil_index", methods={"GET"})
     */
     public function getMyProfile(ProfilRepository $profilRepository, PaginatorInterface $paginator, Request $request): Response
     {
         $profils = $paginator->paginate(
             $profilRepository->findByUser($this->getUser()),
             $request->query->getInt('page', 1),
             5
         );
 
         return $this->render('profil/myprofil.html.twig', ['profils' => $profils]);
     }

    /**
     * @Route("/new", name="profil_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $profil = new Profil();
        $form = $this->createForm(ProfilType::class, $profil);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $profil->setIdUser($this->getUser());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($profil);
            $entityManager->flush();

            return $this->redirectToRoute('myprofil_index');
        }

        return $this->render('profil/new.html.twig', [
            'profil' => $profil,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="profil_show", methods={"GET"})
     */
    public function show(Profil $profil, ProfilRepository $profilRepository): Response
    {
        $profilUser = $profilRepository->findProfilUser($profil->getId());
        return $this->render('profil/show.html.twig', ['profil' => $profil, 'profilUser' => $profilUser[0]]);
    }

    /**
     * @Route("/myprofil/{id}", name="myprofil_show", methods={"GET"})
     */
     public function myShow(Profil $profil): Response
     {
        if($profil->getIdUser()->getId() == $this->getUser()->getId()) {
            return $this->render('profil/myshow.html.twig', ['profil' => $profil]);
        } else {
            return $this->render('404.html.twig');
        }
     }

    /**
     * @Route("/{id}/edit", name="profil_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Profil $profil): Response
    {
        if($profil->getIdUser()->getId() == $this->getUser()->getId()) {
            $form = $this->createForm(ProfilType::class, $profil);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $profil->setUpdatedAt(new DateTime());

                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('myprofil_show', ['id' => $profil->getId()]);
            }

            return $this->render('profil/edit.html.twig', [
                'profil' => $profil,
                'form' => $form->createView(),
            ]);
        } else {
            return $this->render('404.html.twig');
        }
    }

    /**
     * @Route("/{id}", name="profil_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Profil $profil): Response
    {
        if ($this->isCsrfTokenValid('delete'.$profil->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($profil);
            $entityManager->flush();
        }

        return $this->redirectToRoute('myprofil_index');
    }
}
