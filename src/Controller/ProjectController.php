<?php

namespace App\Controller;
use App\Service\NotificationService;
use App\Service\ProjectRoleService;
use App\Entity\Project;
use App\Entity\UserProjectRole;
use App\Entity\UserProjectApply;
use App\Form\UserProjectApplyType;
use App\Form\ProjectType;
use App\Repository\ProjectRepository;
use App\Repository\UserProjectRoleRepository;
use App\Repository\ProfilRepository;
use App\Repository\UserProjectApplyRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use \Datetime;

/**
 * @Route("/project")
 */
class ProjectController extends AbstractController
{
    /**
     * @Route("/", name="project_index", methods={"GET","POST"})
     */
    public function index(ProjectRepository $projectRepository, NotificationService $notificationService, UserProjectApplyRepository $userProjectApplyRepository, ProfilRepository $profileRepository, UserProjectRoleRepository $userProjectRoleRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $projects = $paginator->paginate(
            $projectRepository->findAll(),
            $request->query->getInt('page', 1),
            5
        );

        $alreadyApplys = $userProjectApplyRepository->findPersonalAll($this->getUser());
        $personnalProjects = $userProjectRoleRepository->findPersonalAll($this->getUser());
        $personnalOwnedProject = $userProjectRoleRepository->findPersonalAllIsOwner($this->getUser());

        foreach ($projects as $project){
            $project->cantPostulate = 0;
            $project->isOwner = 0;
            foreach ($personnalProjects as $personnalProject)
                if($project->getId() == $personnalProject->getProject()->getId()){
                    $project->cantPostulate = 1;
                    $project->isOwner = 1;

                }

            foreach ($alreadyApplys as $alreadyApply)
                if($project->getId() == $alreadyApply->getProject()->getId()){
                    $project->cantPostulate = 1;
                }
        }

        $profiles = $profileRepository->findProfileCollab($this->getUser()->getId());

        return $this->render('project/index.html.twig', ['projects' => $projects, 'profiles' => $profiles, 'personalProjects' => $personnalProjects]);
    }

    /**
     * @Route("/myproject", name="myproject_index", methods={"GET"})
     */
    public function getMyProject(ProfilRepository $profileRepository, UserProjectRoleRepository $userProjectRoleRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $projects = $paginator->paginate(
            $userProjectRoleRepository->findPersonalAll($this->getUser()),
            $request->query->getInt('page', 1),
            5
        );

        $profiles = $profileRepository->findProfileRecruteur($this->getUser()->getId());

        return $this->render('project/myproject.html.twig', ['projects' => $projects, 'profiles' => $profiles]);
    }

    /**
     * @Route("/new", name="project_new", methods={"GET","POST"})
     */
    public function new(Request $request, NotificationService $notificationService, ProjectRoleService $projectRoleService, ProfilRepository $profileRepository): Response
    {
        $profile = $profileRepository->findProfileRecruteur($this->getUser()->getId());

        if($profile) {
            $project = new Project();
            $form = $this->createForm(ProjectType::class, $project);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
        
                $projectRole = $projectRoleService->newProjectrole($project, $this->getUser(), 1);

                $notification = $notificationService->newNotification("Vous avez un projet, n'oubliez pas de remplir le plus d'informations", $this->getUser());

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($project);
                $entityManager->persist($projectRole);
                $entityManager->persist($notification);
                $entityManager->flush();

                $this->addFlash('success', 'Le projet a bien été créé');

                return $this->redirectToRoute('myproject_index');
            }
            
            return $this->render('project/new.html.twig', [
                'project' => $project,
                'form' => $form->createView(),
            ]);
        } else {
            return $this->render('404.html.twig');
        }
    }

    /**
     * @Route("/{id}", name="project_show", methods={"GET"})
     */
    public function show(Project $project): Response
    {
        return $this->render('project/show.html.twig', ['project' => $project]);
    }

    /**
     * @Route("/myproject/{id}", name="myproject_show", methods={"GET"})
     */
     public function myShow(Project $project, UserProjectApplyRepository $userProjectApplyRepository, UserProjectRoleRepository $UserProjectRoleRepository): Response
     {
        $projectRole = $UserProjectRoleRepository->findProjectOwner($project->getId());

        if($projectRole[0]->getIdUser()->getId() == $this->getUser()->getId()) {
            $projectApplyNotValidate = $userProjectApplyRepository->findByProjectNotValidate($project->getId());
            $projectApplyIsValidate = $userProjectApplyRepository->findByProjectIsValidate($project->getId());

            return $this->render('project/myshow.html.twig', ['project' => $project, 'project_apply_not_validate' => $projectApplyNotValidate, 'project_apply_is_validate' => $projectApplyIsValidate]);
        } else {
            return $this->render('404.html.twig');
        }
     }

    /**
     * @Route("/{id}/edit", name="project_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Project $project, UserProjectRoleRepository $UserProjectRoleRepository): Response
    {
        $projectRole = $UserProjectRoleRepository->findProjectOwner($project->getId());

        if($projectRole[0]->getIdUser()->getId() == $this->getUser()->getId()) {
            $form = $this->createForm(ProjectType::class, $project);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $project->setUpdatedAt(new DateTime());

                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('success', 'Le projet a bien été modifié');

                return $this->redirectToRoute('myproject_show', ['id' => $project->getId()]);
            }

            return $this->render('project/edit.html.twig', [
                'project' => $project,
                'form' => $form->createView(),
            ]);
        } else {
            return $this->render('404.html.twig');
        }
    }

    /**
     * @Route("/{id}", name="project_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Project $project): Response
    {
        if ($this->isCsrfTokenValid('delete'.$project->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($project);
            $entityManager->flush();
            $this->addFlash('success', 'Le projet a bien été supprimé');
        }

        return $this->redirectToRoute('myproject_index');
    }

    /**
	 * @Route("/{id}", name="project_postulation", methods={"POST"})
	 */
    public function postulation(Project $project, NotificationService $notificationService, ProfilRepository $profileRepository, UserProjectRoleRepository $UserProjectRoleRepository)
    {
        $profil = $profileRepository->findProfileCollab($this->getUser()->getId());
        $idOwnerProject = $UserProjectRoleRepository->findProjectOwner($project->getId());
        $notification = $notificationService->newNotification("Le profil <a href=\"/profil/".$profil[0]->getId()."\">".$this->getUser()->getUsername()."</a> a postulé à votre projet", $idOwnerProject[0]->getIdUser());

        $userProjectApply = new UserProjectApply();
        $userProjectApply->setIdUser($this->getUser());
        $userProjectApply->setProject($project);
        $userProjectApply->setMessageUser("Je souhaite postuler à votre projet");
        $userProjectApply->setApplyDate(new DateTime());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($userProjectApply);
        $entityManager->persist($notification);
        $entityManager->flush();

        return $this->redirectToRoute('project_index');
        // return $this->render('404.html.twig', ['test' => $project]);
    }

}
