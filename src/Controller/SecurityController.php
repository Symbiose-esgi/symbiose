<?php

namespace App\Controller;

use App\Entity\Profil;
use App\Security\MailerService;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\HttpFoundation\Session\Session;


use Swift_Mailer;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Form\UserType;
use App\Form\UserRegisterType;
use App\Form\UserAdminType;
use App\Form\UserMailType;
use App\Form\UserResetPasswordType;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Repository\UserProjectRoleRepository;

use Knp\Component\Pager\PaginatorInterface;



class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();


        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }


    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request,Swift_Mailer $mailer, UserPasswordEncoderInterface $passwordEncoder,UserRepository $userRepository,TokenGeneratorInterface $tokenGenerator): Response
    {
        $user = new User();
        $profil = new Profil();
        $form = $this->createForm(UserRegisterType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->setToken($tokenGenerator->generateToken());
            $user->setIsEnable(false);

            //Insertion de l'utilisateur
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $profil->setIdUser($user);
            $profil->setTitle("Profil collaborateur");
            $profil->setDescription("Profil collaborateur");
            $profil->setRoleProfil(false);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($profil);
            $entityManager->flush();

            //dump($user->getEmail());;
            $tokenGenerator->generateToken();
            //dump($user);
            //Envoyer un mail
            $message = (new \Swift_Message('Hello world'))
            ->setSubject('Symbiose | Confirmation de votre compte')
            ->setFrom('symbiose.esgi@gmail.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                    'emails/confirm-account.html.twig',
                    [
                        'name' => $user->getUsername(),
                        'token'=> $user->getToken()
                    ]
                ),
                'text/html'
            );

            $mailer->send($message);


            return $this->render(
                'security/send_confirmation.html.twig',
                [
                 'message' => "Merci ".$user->getUsername().". Un mail vous a été envoyé à l'adresse ".$user->getEmail()." afin d'activer votre compte Symbiose. Cela nous indiquera 
                 qu'il s'agit bien de vous.",
                 ]);
        }

        return $this->render(
            'security/register.html.twig',
            array('form' => $form->createView())
        );

   
    }

    /**
     * @Route("/activate-account/{token}", name="activate_account", methods="GET")
     */
    public function activateAccount(UserRepository $userRepository,User $user): Response
    {
        //dump($request);
        $userRepository->enableUser($user->getEmail());
        return $this->render('security/account-activate.html.twig',
            [
                'name'=>$user->getUsername()
            ]);

    }


    /**
     * @Route("user/", name="user_index", methods="GET")
     */
    public function index(UserRepository $userRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $users = $paginator->paginate(
            $userRepository->findAll(),
            $request->query->getInt('page', 1),
            3
        );

        $user = $this->getUser();
        return $this->render('user/index.html.twig', ['users' => $users, 'user' => $user]);
    }

    /**
     * @Route("user/new", name="user_new", methods="GET|POST")
     */
    public function new(Request $request , UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $form = $this->createForm(UserAdminType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setUpdatedAt(new \DateTime('now'));
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->setIsEnable(1);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("user/{id}", name="user_show", methods="GET")
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', ['user' => $user]);
    }

    /**
     * @Route("user/{id}/edit", name="user_edit", methods="GET|POST")
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $user->setUpdatedAt(new \DateTime('now'));
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index', ['id' => $user->getId()]);
        }
        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("user/{id}", name="user_delete", methods="DELETE")
     */
    public function delete(Request $request, User $user, UserProjectRoleRepository $userProjectRoleRepository): Response
    {
        $currentUserId = $this->getUser()->getId();
        if ($currentUserId == $user->getId())
        {
            $session = $this->get('session');
            $session = new Session();
            $session->invalidate();
        }
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {

            //rm aussi les projets lié si on est owner
            $allUserProject = $userProjectRoleRepository->findPersonalAllIsOwner($user);

            $em = $this->getDoctrine()->getManager();
            foreach ($allUserProject as $userProject) {
                $em->remove($userProject->getProject());
            }
            $em->remove($user);
            $em->flush();
        }

        return $this->redirectToRoute('app_default_index');
    }

    /**
     * @Route("/confirm-account", name="app_confirm_account")
     */
    public function confirmAccount(Request $request,Swift_Mailer $mailer): Response
    {
        //email
        $newUser = $request->request->get('email');
        if(newUser){
            $message = (new \Swift_Message('Hello world'))
                ->setSubject('Symbiose | Confirmation de votre compte')
                ->setFrom('symbiose.esgi@gmail.com')
                ->setTo($newUser)
                ->setBody(
                    $this->renderView(
                        'emails/forgot.html.twig',
                        ['name' => "Support Symbiose"]
                    ),
                    'text/html'
                );

                $mailer->send($message);

                return $this->render(
                    'security/forgot-password.html.twig',
                    [
                     'message' => "Merci ".$emailUser->getUsername().", un mail vous a été envoyé . Vous pouvez y accéder afin de changer votre mot de passe.",
                     'form' => $form->createView()
                     ] 
                ); 
        }   
    }


    /**
     * @Route("/forgot-password", name="app_forgot_password")
     */
    public function forgotPassword(Request $request,Swift_Mailer $mailer,UserRepository $userRepository,TokenGeneratorInterface $tokenGenerator): Response
    {
        $form = $this->createForm(UserMailType::class); 
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $emailUser = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneByMail($request->request->get('user_mail')["email"]);

            if($emailUser){
                $token = $tokenGenerator->generateToken();
                $emailUser->setToken($token);
                
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($emailUser);
                $entityManager->flush();
 
                $message = (new \Swift_Message('Hello world'))
                ->setSubject('Mot de passe oublié')
                ->setFrom('symbiose.esgi@gmail.com')
                ->setTo($emailUser->getEmail())
                ->setBody(
                    $this->renderView(
                        'emails/forgot.html.twig',
                        ['name' => "Support Symbiose",'token'=>$token]),
                    'text/html'
                );

                $mailer->send($message);

                return $this->render(
                    'security/forgot-password.html.twig',
                    [
                     'message' => "Merci ".$emailUser->getUsername().", un mail vous a été envoyé . Vous pouvez y accéder afin de changer votre mot de passe.",
                     'form' => $form->createView()
                     ] 
                );   
            }

        }

        return $this->render(
            'security/forgot-password.html.twig',
            array('form' => $form->createView(),'message'=>null)
        );

       
    }

       /**
     * @Route("/reset-password/{token}", name="reset-password", methods="GET")
     */
    public function resetPassword(Request $request,UserRepository $userRepository,User $user, UserPasswordEncoderInterface $passwordEncoder,UserResetPasswordType $resetPasswordType): Response
    {

        $form = $this->createForm(UserResetPasswordType::class,$user); 
        //dump($request);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            
            $newPassword = $request->request->get('password');
            //$confirmPassword = $request->request->get('newpassword');


            //if($newPassword == $confirmPassword){
                
                $user = $this->getDoctrine()
                ->getRepository(User::class)
                ->findOneByToken($request->attributes->get('token'));

                $newEncodedPassword = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($newEncodedPassword);
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

            /*} else{
                return $this->render(
                    'security/reset-password.html.twig',
                    [
                     'message' => "Vos deux mot de passes sont différents...",
                     'form' => $form->createView()
                     ] 
                );  
            }*/

        }

        return $this->render(
            'security/reset-password.html.twig',
            [
             'message' => "Hello ".$user->getUsername().", vous pouvez créer votre nouveau mot de passe ici.",
             'form' => $form->createView()
             ] 
        );   

  
    }

}
