<?php

namespace App\Controller;

use App\Service\NotificationService;
use App\Controller\ProjectController;
use App\Entity\UserProjectApply;
use App\Form\UserProjectApplyType;
use App\Repository\UserProjectApplyRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use \Datetime;

/**
 * @Route("/apply")
 */
class UserProjectApplyController extends AbstractController
{
	/**
	 * @Route("/", name="user_project_apply_index", methods={"GET"})
	 */
	public function index(UserProjectApplyRepository $userProjectApplyRepository): Response
	{
		return $this->render('user_project_apply/index.html.twig', [
			'user_project_applies' => $userProjectApplyRepository->findAll(),
		]);
	}

	/**
	 * @Route("/new", name="user_project_apply_new", methods={"GET","POST"})
	 */
	public function new(Request $request): Response
	{
		$userProjectApply = new UserProjectApply();
		$form = $this->createForm(UserProjectApplyType::class, $userProjectApply);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist($userProjectApply);
			$entityManager->flush();

			return $this->redirectToRoute('user_project_apply_index');
		}

		return $this->render('user_project_apply/new.html.twig', [
			'user_project_apply' => $userProjectApply,
			'form' => $form->createView(),
		]);
	}

	/**
	 * @Route("/{id}", name="user_project_apply_show", methods={"GET"})
	 */
	public function show(UserProjectApply $userProjectApply): Response
	{
		return $this->render('user_project_apply/show.html.twig', [
			'user_project_apply' => $userProjectApply,
		]);
	}

	/**
	 * @Route("/{id}/edit", name="user_project_apply_edit", methods={"GET","POST"})
	 */
	public function edit(Request $request, UserProjectApply $userProjectApply): Response
	{
		$form = $this->createForm(UserProjectApplyType::class, $userProjectApply);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->getDoctrine()->getManager()->flush();

			return $this->redirectToRoute('user_project_apply_index', [
				'id' => $userProjectApply->getId(),
			]);
		}

		return $this->render('user_project_apply/edit.html.twig', [
			'user_project_apply' => $userProjectApply,
			'form' => $form->createView(),
		]);
	}

	/**
	 * @Route("/{id}", name="user_project_apply_delete", methods={"DELETE"})
	 */
	public function delete(Request $request, UserProjectApply $userProjectApply, NotificationService $notificationService): Response
	{
		$project = $userProjectApply->getProject();

		if ($this->isCsrfTokenValid('delete'.$userProjectApply->getId(), $request->request->get('_token'))) {
			$notification = $notificationService->newNotification("Vous avez récemment postulé pour participer au projet : <a href=\"/project/".$project->getId()."\">ICI</a> <BR> Votre requête a malheureusement été refusé.", $userProjectApply->getIdUser());
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->remove($userProjectApply);
			$entityManager->persist($notification);
			$entityManager->flush();
		}

		return $this->redirectToRoute('myproject_show', ['id' => $project->getId()]);
	}

	/**
	 * @Route("/{id}", name="user_project_apply_accept", methods={"POST"})
	 */
	public function accept(Request $request, UserProjectApply $userProjectApply, NotificationService $notificationService): Response
	{
		$project = $userProjectApply->getProject();

		//Send notification
		$notification = $notificationService->newNotification("Vous avez récemment postulé pour participer au projet : <a href=\"/project/".$project->getId()."\">ICI</a> <BR> Votre requête a été accepté. Vous faites désormais partie du projet.", $userProjectApply->getIdUser());

		$entityManager = $this->getDoctrine()->getManager();

    	$userProjectApply->setIsValidate(true);

		$entityManager->persist($userProjectApply);
		$entityManager->persist($notification);
    	$entityManager->flush();

		return $this->redirectToRoute('myproject_show', ['id' => $project->getId()]);
	}

}
