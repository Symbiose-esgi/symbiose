<?php

namespace App\DataFixtures;

use App\Entity\Project;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class ProjectFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
    	$faker = Factory::create('fr_FR');
    	for($i = 0; $i < 50; $i++){
    		$project = new Project();
    		$project
    			->setName($faker->words(1, true))
    			->setDescription($faker->sentences(3, true))
    			->setDescriptionPrivate($faker->sentences(3, true))
    			->setIsPrivate($faker->boolean(50))
    			->setIsPremium($faker->boolean(50))
    		;
    		$manager->persist($project);
    	}
        $manager->flush();
    }
}
