<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use \Datetime;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $descriptionPrivate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPrivate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPremium;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Document", mappedBy="project", cascade={"remove"})
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserProjectRole", mappedBy="project", cascade={"remove"})
     */
    private $userProjectRoles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserProjectApply", mappedBy="project", cascade={"remove"})
     */
    private $userProjectApplies;

    public function __construct()
    {
        $this->documents = new ArrayCollection();
        $this->userProjectRoles = new ArrayCollection();
        $this->userProjectApplies = new ArrayCollection();
        $this->createdAt = new DateTime();
        $this->updatedAt = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescriptionPrivate(): ?string
    {
        return $this->descriptionPrivate;
    }

    public function setDescriptionPrivate(?string $descriptionPrivate): self
    {
        $this->descriptionPrivate = $descriptionPrivate;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIsPrivate(): ?bool
    {
        return $this->isPrivate;
    }

    public function setIsPrivate(bool $isPrivate): self
    {
        $this->isPrivate = $isPrivate;

        return $this;
    }

    public function getIsPremium(): ?bool
    {
        return $this->isPremium;
    }

    public function setIsPremium(bool $isPremium): self
    {
        $this->isPremium = $isPremium;

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setProject($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->contains($document)) {
            $this->documents->removeElement($document);
            // set the owning side to null (unless already changed)
            if ($document->getProject() === $this) {
                $document->setProject(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserProjectRole[]
     */
    public function getUserProjectRoles(): Collection
    {
        return $this->userProjectRoles;
    }

    public function addUserProjectRole(UserProjectRole $userProjectRole): self
    {
        if (!$this->userProjectRoles->contains($userProjectRole)) {
            $this->userProjectRoles[] = $userProjectRole;
            $userProjectRole->setProject($this);
        }

        return $this;
    }

    public function removeUserProjectRole(UserProjectRole $userProjectRole): self
    {
        if ($this->userProjectRoles->contains($userProjectRole)) {
            $this->userProjectRoles->removeElement($userProjectRole);
            // set the owning side to null (unless already changed)
            if ($userProjectRole->getProject() === $this) {
                $userProjectRole->setProject(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserProjectApply[]
     */
    public function getUserProjectApplies(): Collection
    {
        return $this->userProjectApplies;
    }

    public function addUserProjectApply(UserProjectApply $userProjectApply): self
    {
        if (!$this->userProjectApplies->contains($userProjectApply)) {
            $this->userProjectApplies[] = $userProjectApply;
            $userProjectApply->setProject($this);
        }

        return $this;
    }

    public function removeUserProjectApply(UserProjectApply $userProjectApply): self
    {
        if ($this->userProjectApplies->contains($userProjectApply)) {
            $this->userProjectApplies->removeElement($userProjectApply);
            // set the owning side to null (unless already changed)
            if ($userProjectApply->getProject() === $this) {
                $userProjectApply->setProject(null);
            }
        }

        return $this;
    }
}
