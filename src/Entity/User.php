<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields="email", message="Email already taken")
 * @UniqueEntity(fields="username", message="Username already taken")
 * @ORM\Table(name="user_account")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank
     * @Assert\Email
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @Assert\NotBlank
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $userLastName;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPremium;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnable=false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Profil", mappedBy="idUser", cascade={"remove"})
     */
    private $profils;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Chat", inversedBy="users", cascade={"remove"})
     */
    private $chat;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Message", mappedBy="idUser", cascade={"remove"})
     */
    private $messages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserProjectRole", mappedBy="idUser", cascade={"remove"})
     */
    private $userProjectRoles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserProjectApply", mappedBy="idUser", cascade={"remove"})
     */
    private $userProjectApplies;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Notification", mappedBy="idUser", cascade={"remove"})
     */
    private $notifications;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token;

    public function __construct()
    {
        $this->profils = new ArrayCollection();
        $this->chat = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->userProjectRoles = new ArrayCollection();
        $this->userProjectApplies = new ArrayCollection();
        $this->createdAt = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');
        $this->notifications = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }



    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }


    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getUserLastName(): ?string
    {
        return $this->userLastName;
    }

    public function setUserLastName(string $userLastName): self
    {
        $this->userLastName = $userLastName;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        if (is_null($this->getCreatedAt())){
            $this->createdAt = new \DateTime('now');
        }

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = new \DateTime('now');

        return $this;
    }

    public function getIsPremium(): ?bool
    {
        return $this->isPremium;
    }

    public function setIsPremium(bool $isPremium): self
    {

        $this->isPremium = $isPremium;


        return $this;
    }

    public function getIsEnable(): ?bool
    {
        return $this->isEnable;
    }

        public function setIsEnable(bool $isEnable): self
    {

        $this->isEnable = $isEnable;

        return $this;
    }

        /**
         * @return Collection|Profil[]
         */
        public function getProfils(): Collection
        {
            return $this->profils;
        }

        public function addProfil(Profil $profil): self
        {
            if (!$this->profils->contains($profil)) {
                $this->profils[] = $profil;
                $profil->setIdUser($this);
            }

            return $this;
        }

        public function removeProfil(Profil $profil): self
        {
            if ($this->profils->contains($profil)) {
                $this->profils->removeElement($profil);
                // set the owning side to null (unless already changed)
                if ($profil->getIdUser() === $this) {
                    $profil->setIdUser(null);
                }
            }

            return $this;
        }

        /**
         * @return Collection|Chat[]
         */
        public function getChat(): Collection
        {
            return $this->chat;
        }

        public function addChat(Chat $chat): self
        {
            if (!$this->chat->contains($chat)) {
                $this->chat[] = $chat;
            }

            return $this;
        }

        public function removeChat(Chat $chat): self
        {
            if ($this->chat->contains($chat)) {
                $this->chat->removeElement($chat);
            }

            return $this;
        }

        /**
         * @return Collection|Message[]
         */
        public function getMessages(): Collection
        {
            return $this->messages;
        }

        public function addMessage(Message $message): self
        {
            if (!$this->messages->contains($message)) {
                $this->messages[] = $message;
                $message->setIdUser($this);
            }

            return $this;
        }

        public function removeMessage(Message $message): self
        {
            if ($this->messages->contains($message)) {
                $this->messages->removeElement($message);
                // set the owning side to null (unless already changed)
                if ($message->getIdUser() === $this) {
                    $message->setIdUser(null);
                }
            }

            return $this;
        }

        /**
         * @return Collection|UserProjectRole[]
         */
        public function getUserProjectRoles(): Collection
        {
            return $this->userProjectRoles;
        }

        public function addUserProjectRole(UserProjectRole $userProjectRole): self
        {
            if (!$this->userProjectRoles->contains($userProjectRole)) {
                $this->userProjectRoles[] = $userProjectRole;
                $userProjectRole->setIdUser($this);
            }

            return $this;
        }

        public function removeUserProjectRole(UserProjectRole $userProjectRole): self
        {
            if ($this->userProjectRoles->contains($userProjectRole)) {
                $this->userProjectRoles->removeElement($userProjectRole);
                // set the owning side to null (unless already changed)
                if ($userProjectRole->getIdUser() === $this) {
                    $userProjectRole->setIdUser(null);
                }
            }

            return $this;
        }

        /**
         * @return Collection|UserProjectApply[]
         */
        public function getUserProjectApplies(): Collection
        {
            return $this->userProjectApplies;
        }

        public function addUserProjectApply(UserProjectApply $userProjectApply): self
        {
            if (!$this->userProjectApplies->contains($userProjectApply)) {
                $this->userProjectApplies[] = $userProjectApply;
                $userProjectApply->setIdUser($this);
            }

            return $this;
        }

        public function removeUserProjectApply(UserProjectApply $userProjectApply): self
        {
            if ($this->userProjectApplies->contains($userProjectApply)) {
                $this->userProjectApplies->removeElement($userProjectApply);
                // set the owning side to null (unless already changed)
                if ($userProjectApply->getIdUser() === $this) {
                    $userProjectApply->setIdUser(null);
                }
            }

            return $this;
        }

        /**
         * @return Collection|Notification[]
         */
        public function getNotifications(): Collection
        {
            return $this->notifications;
        }

        public function addNotification(Notification $notification): self
        {
            if (!$this->notifications->contains($notification)) {
                $this->notifications[] = $notification;
                $notification->setIdUser($this);
            }

            return $this;
        }

        public function removeNotification(Notification $notification): self
        {
            if ($this->notifications->contains($notification)) {
                $this->notifications->removeElement($notification);
                // set the owning side to null (unless already changed)
                if ($notification->getIdUser() === $this) {
                    $notification->setIdUser(null);
                }
            }

            return $this;
        }

        public function getToken(): ?string
        {
            return $this->token;
        }

        public function setToken(?string $token): self
        {
            $this->token = $token;

            return $this;
        }
        
}
