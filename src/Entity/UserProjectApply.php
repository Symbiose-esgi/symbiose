<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserProjectApplyRepository")
 */
class UserProjectApply
{
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $messageUser;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $applyDate;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $answerDate;

	/**
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	private $isValidate;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userProjectApplies")
	 */
	private $idUser;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="userProjectApplies")
	 */
	private $project;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getMessageUser(): ?string
	{
		return $this->messageUser;
	}

	public function setMessageUser(string $messageUser): self
	{
		$this->messageUser = $messageUser;

		return $this;
	}

	public function getApplyDate(): ?\DateTimeInterface
	{
		return $this->applyDate;
	}

	public function setApplyDate(\DateTimeInterface $applyDate): self
	{
		$this->applyDate = $applyDate;

		return $this;
	}

	public function getAnswerDate(): ?\DateTimeInterface
	{
		return $this->answerDate;
	}

	public function setAnswerDate(\DateTimeInterface $answerDate): self
	{
		$this->answerDate = $answerDate;

		return $this;
	}

	public function getIsValidate(): ?bool
	{
		return $this->isValidate;
	}

	public function setIsValidate(bool $isValidate): self
	{
		$this->isValidate = $isValidate;

		return $this;
	}

	public function getIdUser(): ?User
	{
		return $this->idUser;
	}

	public function setIdUser(?User $idUser): self
	{
		$this->idUser = $idUser;

		return $this;
	}

	public function getProject(): ?Project
	{
		return $this->project;
	}

	public function setProject(?Project $project): self
	{
		$this->project = $project;

		return $this;
	}
}
