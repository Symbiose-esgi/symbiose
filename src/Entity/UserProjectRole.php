<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserProjectRoleRepository")
 */
class UserProjectRole
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="boolean")
     */
    private $isOwner;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userProjectRoles")
     */
    private $idUser;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="userProjectRoles")
     */
    private $project;

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getIsOwner(): ?bool
    {
        return $this->isOwner;
    }

    public function setIsOwner(bool $isOwner): self
    {
        $this->isOwner = $isOwner;

        return $this;
    }

    public function getIdUser(): ?User
    {
        return $this->idUser;
    }

    public function setIdUser(?User $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }
}
