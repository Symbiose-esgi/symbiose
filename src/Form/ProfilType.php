<?php

namespace App\Form;

use App\Entity\Profil;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProfilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Title', TextType::class,[
                'label' => 'Titre',
            ])
            ->add('description')
            ->add('website', TextType::class,[
                'label' => 'Site web',
                'required' => false
            ])
            ->add('linkedin', TextType::class,[
                'required' => false
            ])
            ->add('roleProfil', CheckboxType::class,[
                'label' => 'Recruteur',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Profil::class,
            'translation_domain' => 'profile'
        ]);
    }
}
