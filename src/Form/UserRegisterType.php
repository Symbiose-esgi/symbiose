<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class UserRegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, array(
                'attr' => array(
                    'placeholder' => 'Email',
                )))
            ->add('username', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Identifiant',
                )))
            ->add('userLastName', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Nom',
                )))
            ->add('isPremium', HiddenType::class, [
                'empty_data' => 0,
            ])
            ->add('isEnable', HiddenType::class, [
                'empty_data' => 0,
            ])
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options' => array('label' => 'Password', 'attr' => array(
                    'placeholder' => 'Mot de passe'
                )),
                'second_options' => array('label' => 'Repeat Password', 'attr' => array(
                    'placeholder' => 'Saisir de nouveau le mot de passe'
                )),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
            'translation_domain' => 'forms'
        ));
    }
}