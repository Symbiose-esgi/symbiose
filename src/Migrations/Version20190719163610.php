<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190719163610 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE profil ALTER title DROP NOT NULL');
        $this->addSql('ALTER TABLE profil ALTER description DROP NOT NULL');
        $this->addSql('ALTER TABLE profil ALTER role_profil DROP NOT NULL');
        $this->addSql('ALTER TABLE project ALTER is_private DROP NOT NULL');
        $this->addSql('ALTER TABLE project ALTER is_premium DROP NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE profil ALTER title SET NOT NULL');
        $this->addSql('ALTER TABLE profil ALTER description SET NOT NULL');
        $this->addSql('ALTER TABLE profil ALTER role_profil SET NOT NULL');
        $this->addSql('ALTER TABLE project ALTER is_private SET NOT NULL');
        $this->addSql('ALTER TABLE project ALTER is_premium SET NOT NULL');
    }
}
