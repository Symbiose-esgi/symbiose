<?php

namespace App\Repository;

use App\Entity\Profil;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Profil|null find($id, $lockMode = null, $lockVersion = null)
 * @method Profil|null findOneBy(array $criteria, array $orderBy = null)
 * @method Profil[]    findAll()
 * @method Profil[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProfilRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Profil::class);
    }

    // /**
    //  * @return Profil[] Returns an array of Profil objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findAllProfileCollab()
    {
        return $this->createQueryBuilder('p')
            ->innerJoin('p.idUser', 'u')
            ->addSelect('u')
            ->andWhere('p.roleProfil = false')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findProfileCollab($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.idUser = :val')
            ->andWhere('p.roleProfil = false')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findProfileRecruteur($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.idUser = :val')
            ->andWhere('p.roleProfil = true')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByUser($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.idUser = :val')
            ->setParameter('val', $value)
            ->orderBy('n.updatedAt', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findProfilUser($value)
    {
        return $this->createQueryBuilder('p')
            ->innerJoin('p.idUser', 'u')
            ->addSelect('u')
            ->andWhere('p.id = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
            ;
    }
}
