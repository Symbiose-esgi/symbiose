<?php

namespace App\Repository;

use App\Entity\UserProjectApply;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserProjectApply|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserProjectApply|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserProjectApply[]    findAll()
 * @method UserProjectApply[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserProjectApplyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserProjectApply::class);
    }

    // /**
    //  * @return UserProjectApply[] Returns an array of UserProjectApply objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserProjectApply
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findByProjectNotValidate($value)
    {
        return $this->createQueryBuilder('p')
            ->innerJoin('p.idUser', 'u')
            ->innerJoin('u.profils', 'f')
            ->addSelect('u, f')
            ->andWhere('p.project = :val')
            ->andWhere('p.isValidate IS NULL')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByProjectIsValidate($value)
    {
        return $this->createQueryBuilder('p')
            ->innerJoin('p.idUser', 'u')
            ->innerJoin('u.profils', 'f')
            ->addSelect('u, f')
            ->andWhere('p.project = :val')
            ->andWhere('p.isValidate = true')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findPersonalAll($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.idUser = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
            ;
    }
}
