<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findOneByMail($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.email = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function disableUser($mail): ?User 
    {
        return $this->update('u')
            ->set('u.isnable', FALSE)
            ->where('u.email = :val' )
            ->setParameter(1, $mail);
    }

    public function createTokenUser($mail,$token): ?User 
    {
        return $this->createQueryBuilder('u')
            ->update('u')
            ->set('u.token', $token)
            ->where('u.email = :val')
            ->setParameter(1, $mail)
            ->getQuery()
            ->getOneOrNullResult();
    }


    public function isEnable($mail): ?User
    {
       return $this->createQueryBuilder('u')
        ->where('u.isEnable = :bool')
        ->andWhere('u.email = :mail')
        ->setParameter('bool', false)
        ->setParameter('mail', $mail)
        ->getQuery()
        ->getOneOrNullResult();
    }

    public function enableUser($mail){
        return $this->getEntityManager()->createQueryBuilder()
        ->update('u')
        ->set('u.is_enable', TRUE)
        ->where('u.email = :val')
        ->setParameter(1, $mail);
    }

    public function findOneByToken($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.toekn = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
