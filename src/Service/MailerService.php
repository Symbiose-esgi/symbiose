<?php


namespace App\Service;

use Swift_Mailer;


class MailerService
{

    public function sendConfirmation($addresse,$username)
    {
        $message = (new \Swift_Message('Hello world'))
        ->setSubject('Symbiose | Confirmation de votre compte')
        ->setFrom('symbiose.esgi@gmail.com')
        ->setTo($addresse)
        ->setBody(
            $this->renderView(
                'emails/confirm-account.html.twig',
                ['name' => $username]
            ),
            'text/html'
        );

        $mailer->send($message);


    }
}

