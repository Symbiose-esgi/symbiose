<?php

namespace App\Service;

use App\Entity\Notification;

class NotificationService
{
    public function newNotification($message, $user)
    {
        $notification = new Notification();
        $notification->setMessage($message);
        $notification->setIdUser($user);
        $notification->setIsSeen(0);
        $notification->setCreatedAt(new \DateTime('now'));
        return $notification;
    }
}

