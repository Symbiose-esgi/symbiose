<?php


namespace App\Service;

use App\Entity\UserProjectRole;


class ProjectRoleService
{

    public function newProjectRole($project, $user, $isOwner)
    {
        $projectRole = new UserProjectRole();
        $projectRole->setIsOwner($isOwner);
        $projectRole->setIdUser($user);
        $projectRole->setProject($project);
        return $projectRole;

    }
}

